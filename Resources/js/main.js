(function () {
    "use strict";
	var MoreDataTV = {};
	MoreDataTV.client = Titanium.Network.createHTTPClient();
	MoreDataTV.page = null;
	MoreDataTV.callPage = function (pageNumber) {
		MoreDataTV.page = "";
		if (MoreDataTV.client.open("GET", "http://nodata.tv/page/" + pageNumber.toString())) {
			Titanium.API.info("opened ok");
			MoreDataTV.client.receive(function (response) {
				MoreDataTV.page += response.toString();
			}, null);
		} else {
			Titanium.API.info('cannot open connection');
	        MoreDataTV.page = null;
		}
	};

	$(function () {
		MoreDataTV.client.onreadystatechange = function () {
			if (MoreDataTV.client.readyState === 4) {
				var pageResult = $(MoreDataTV.page);
				$("#result").html(pageResult.find('[class|="post"]'));
			}
		};
		MoreDataTV.callPage(1);
	});
})();